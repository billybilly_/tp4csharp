using System;
using Xunit;
using Mesozoic;

namespace MesozoicTest
{
        public class DinosaurTest
    {

    public void TestDinosaurConstructor()
    {
    Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

    Assert.Equal("Louis", louis.name);
    Assert.Equal("Stegausaurus", louis.specie);
    Assert.Equal(12, louis.age);
    }


    public void TestDinosaurRoar()
    {
        Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
        Assert.Equal("Grrr", louis.roar());
    }


    public void TestDinosaurSayHello()
    {
        Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
        Assert.Equal("Je suis Louis le Stegausaurus, j'ai 12 ans.", louis.sayHello());
    }
    }
}
